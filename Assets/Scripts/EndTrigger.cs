﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* 
    This script is attached to a GameObject at the end of level that serves as a trigger.
    On collide, the OnTriggerEnter will be called which tells the GameManager that the level is completed.
*/

public class EndTrigger : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameObject player; // can be used to check if this is triggered by the player or other types of objects


    // On collision enter won't work when 'is trigger' is enabled in BoxCollider
    // Also triggered by other collisions so we can check if the collider is the player itself
    void OnTriggerEnter()
    {
        // Get reference to game manager
        gameManager.CompleteLevel();
        // Get reference to a component/script of the GameObject and call it
        // player.GetComponent<PlayerMovement>().stopMoving();
    }
}
