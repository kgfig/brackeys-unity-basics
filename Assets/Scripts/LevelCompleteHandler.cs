﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/* 
 *  This script is solely responsible for what happens in the background when a level is completed.
 *  Currently, this class is only referenced as an AnimationEvent of the LevelComplete UI animation.
 *  An AnimatonEvent can be used to call public functions of scripts.
 */
public class LevelCompleteHandler : MonoBehaviour
{

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // Use build index to load the next scene
    }
}
