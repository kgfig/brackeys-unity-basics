﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float forwardForce = 2000f;
    [SerializeField] float sidewaysForce = 500f;

    private Vector3 _startPosition;

    public Rigidbody rb; // Linked via UI
    // Start is called before the first frame update
    // When you start the game
    void Start()
    {
        _startPosition = rb.position;
    }

    // Update is called once per frame
    // Unity prefers to call FixedUpdate() for physics update instead of the default Update()
    void FixedUpdate()
    {
        // Higher frame rate lower delta time
        // Time.deltaTime // amount of seconds since last render
        rb.AddForce(0, 0, forwardForce * Time.deltaTime);

        // How to get player input
        // Input considerations: Smoothing player movement, alternative keys, supporting controller

        // Jumping or one-off events are not supposed to be done here in FixedUpdate
        // Input is updated in the Update() method and you might miss some player input here
        // Ideally, check input Update(), store it and update here
        if (Input.GetKey("d"))
        {
            rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }
        if (Input.GetKey("a"))
        {
            rb.AddForce(-sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        // If it falls of the ground reset
        if (rb.position.y < -1)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }

    public void resetPositionAndVelocity()
    // call when you have to reset position and velocity while keeping the rest of the scene in tact
    {
        rb.velocity = Vector3.zero;
        rb.position = _startPosition;
    }

    public void stopMoving()
    {
        rb.velocity = Vector3.zero;
        rb.isKinematic = false;
    }
}
