﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/* 
 * Responsible for managing game states: starting, restarting etc, transition to other menu, display game over etc. 
 */
public class GameManager : MonoBehaviour
{

    private bool isEnded = false;
    [SerializeField] float restartDelay = 2f;
    [SerializeField] GameObject completeLevelUI;

    public void StartGame()
    {
        SceneManager.LoadScene("Level1");
    }

    /* The GameManager also updates the UI when a level is completed.
       The UI object is set active which will start its animation.
       At the end of the animation, an AnimationEvent will be fired and will call the LevelCompleteHandler script.
    */
    public void CompleteLevel()
    {
        if (!isEnded)
        {
            isEnded = true;
            completeLevelUI.SetActive(true);
        }
    }

    public void EndGame()
    {
        if (!isEnded)
        {
            isEnded = true;
            Debug.Log("GAME OVER");
            // 2 ways to call a function with delay: Invoke, StartCoroutine
            // StartCoroutine: call the delay with the function
            // Invoke: Pass the function name and delay
            Invoke("Restart", restartDelay);
        }
    }

    void Restart()
    {
        // Load the current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
