﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [SerializeField] Transform playerTransform;
    [SerializeField] Text scoreText;

    // Update is called once per frame
    void Update()
    {
        // Show current distance travelled as score
        scoreText.text = (playerTransform.position.z * 20).ToString("0");
    }
}
