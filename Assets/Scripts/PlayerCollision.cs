﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    [SerializeField] PlayerMovement movement;

    // Use tags to group objects together
    void OnCollisionEnter(Collision collisionInfo)
    { // Called when object collides with something

        if (collisionInfo.collider.tag == "Obstacle")
        {
            // Disable movement when we hit something
            movement.enabled = false;
            // Instead of creating a reference to the GameManager component
            // where we always have to manually link the script,
            // we use this
            FindObjectOfType<GameManager>().EndGame();
            // StartCoroutine(ResetAfterDelay());
        }

    }

    IEnumerator ResetAfterDelay()
    {
        yield return new WaitForSeconds(3);
        Debug.Log("collided reset");
        movement.enabled = true;
        movement.resetPositionAndVelocity();
    }
}
